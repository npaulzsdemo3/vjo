import pytest


from zpytest.models.smui_models import ZdxAdminRoles, AdminUser
from zpytest.smui.data import constant
from zslib.utils.custom_logger.zlogger import Zlogger

zlogger = Zlogger().customLogger()


@pytest.mark.aga29
@pytest.mark.webtest.with_args(jira=['QA-135907', 'QA-135912', 'QA-135913'],
                               package='AGA29', feature='ZDX-3344')
def test_zcc_ma_app_store(app, data_zdxRbacUi_roles_zccNoneAdminFullViewNone):
    data = data_zdxRbacUi_roles_zccNoneAdminFullViewNone
    zlogger.info("Role tested: {}".format(data))
    username = constant.random_name('testuser_', 8)
    email = constant.random_name('admin', 3) + '@abc.com'
    password = 'Admin@123'
    zdx_role = ZdxAdminRoles(name=constant.random_name('role_', 8), **data)
    zdx_user = AdminUser(username=username,
                         scope='ORGANIZATION', isAuditor=False,
                         email=email,
                         password=password)
    zdx_role, zdx_user, sso_headers = app.api.zdx_sso.zdx_admin_and_role_creation(
        zdx_role, zdx_user)
    app.api.zdx_sso.activate(sso_headers)
    app.start_zdx_ui_session(zdx_user.login_name, password)
    app.zdx_nav_menu.open_navigation_menu('ADMINISTRATION')
    app.zdx_admin.check_ma_page_link_exists()
    app.logout_zdx_ui_session()


